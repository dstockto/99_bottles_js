const gulp = require('gulp');
const shell = require('gulp-shell');
const fs = require('fs');

gulp.task('default', () => {
    fs.writeFile("autocommit_counter", "1", {flag: 'w+'}, () => {
    });
    gulp.watch('**/*.js', ['runtests']);
});

gulp.task(
    'runtests',
    shell.task(
        [
            'CI=true yarn test',
            'git add src/ test/',
            'git commit -m "Auto-commit TDD #`cat autocommit_counter`"',
            'count=`cat autocommit_counter` && let "count++" && echo $count > autocommit_counter'
        ]
    )
);
